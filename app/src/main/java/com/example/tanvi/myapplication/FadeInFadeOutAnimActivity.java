package com.example.tanvi.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class FadeInFadeOutAnimActivity extends AppCompatActivity {
    private Button mButtonFadeIn,mButtonFadeOut,mButtonTransition;
    private ImageView mImageCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fade_in_fade_out_anim);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mButtonFadeIn = findViewById(R.id.button_fadein);
        mButtonFadeOut = findViewById(R.id.button_fadeout);
        mButtonTransition = findViewById(R.id.button_transition);
        mImageCamera = findViewById(R.id.image_camera);

        final Animation animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        mButtonFadeIn.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                mImageCamera.startAnimation(animationFadeIn);
            }
        });

        final Animation animationFadeOut = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        mButtonFadeOut.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mImageCamera.startAnimation(animationFadeOut);

            }
        });

        mButtonTransition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FadeInFadeOutAnimActivity.this, VectorDrawableActivity.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(FadeInFadeOutAnimActivity.this,
                                mImageCamera,
                                ViewCompat.getTransitionName(mImageCamera));
                startActivity(intent, options.toBundle());
            }
        });
    }

}

