package com.example.tanvi.myapplication;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class ObjectAnimatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_animator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(fab, "translationY", 0f, -720);

        objectAnimator.setDuration(1000);
        objectAnimator.setRepeatCount(1);
        objectAnimator.start();

        objectAnimator = ObjectAnimator.ofFloat(fab, "rotation", 0f, 180f);

        objectAnimator.setDuration(1000);
        objectAnimator.start();
    }

}
