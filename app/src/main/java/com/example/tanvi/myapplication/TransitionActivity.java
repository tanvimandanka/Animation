package com.example.tanvi.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Scene;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class TransitionActivity extends AppCompatActivity {
    private Scene current;
    private Scene other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition);
        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);

        current = Scene.getSceneForLayout(container,
                R.layout.scene1_layout,
                this);
        other = Scene.getSceneForLayout(container,
                R.layout.scene2_layout,
                this);
        current.enter();
    }

    public void changeScenes(View view) {
        Scene tmp = other;
        other = current;
        current = tmp;
    
        TransitionManager.go(current);
    }
}
